Performio - Assignment
==========================

This project is done as part of Performio interview process Assignment

Probelm Statment
================

<B>Current Weather</B>

In this assignment, we are going to create a Spring Boot app that shows the weather for a location. You will create a basic API using Spring Boot.

<b>Serving requests</b>

The API should have two endpoints:

* /weather
This endpoint should take one argument: 
location (string) Test data: "Melbourne, AU"
To source the weather data, use one of the APIs available from https://openweathermap.org/. You will need to create an account and get an API key to use in your app.
Your API key should not be made available to us or users of your app. Think about how you can deal with the key as a secret.

* /healthcheck
Set up this endpoint to be a basic healthcheck endpoint for the app.


Run Application
===============
java -jar <jarname> -Dapikey=<open_weather_apikey> -DlogPath=<log_path>

* open_weather_apikey - your open weather account apikey
* log_path - logger file path in the system


API
===
Application context : weathercheck
* /weather?location={location}
* /healthcheck


swagger-ui
==========
swagger-ui is available   

* http://localhost:8080/weathercheck/swagger-ui.html

Developed By
============

* Geevarugehse John <geevarughesejohn@gmail.com>

 