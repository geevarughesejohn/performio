package com.performio.assigment.weathercheck;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

/**
 * Abstract class for unit testing. It extending testing frame work
 * 
 * use by extending.
 * 
 * example : @HealthCheckControllerTest
 * 
 * @author geeva
 *
 */
@SpringBootTest(classes = WeathercheckApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class AbstractWeatherCheckTest extends AbstractTestNGSpringContextTests {

}
