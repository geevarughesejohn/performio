package com.performio.assigment.weathercheck.service.weather.impl;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.web.client.RestTemplate;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.performio.assigment.weathercheck.WeathercheckApplication;
import com.performio.assigment.weathercheck.common.WeatherCheckConstants;
import com.performio.assigment.weathercheck.model.Weather;
import com.performio.assigment.weathercheck.service.weather.impl.OpenWeatherReader;

@SpringBootTest(classes = WeathercheckApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class OpenWeatherReaderTest extends AbstractTestNGSpringContextTests {

	private RestTemplate restTemplate = Mockito.mock(RestTemplate.class);

	@Autowired
	@Qualifier("OpenWeatherReader")
	private OpenWeatherReader openWeatherReader;

	private String apiKey = "apiKeyTest";

	@BeforeClass
	public void beforeClass() throws IllegalAccessException {
		FieldUtils.writeField(openWeatherReader, "restTemplate", restTemplate, true);
		FieldUtils.writeField(openWeatherReader, "apiKey", apiKey, true);

	}

	@Test(dataProvider = "locationProvider")
	public void getWeatherTest(String location, boolean isSucess, String expResult) {
		String requestUrl = openWeatherReader.getCurrentWeatherURI();
		requestUrl = requestUrl.replaceAll(WeatherCheckConstants.CITY_NAME, location);
		requestUrl = requestUrl.replaceAll(WeatherCheckConstants.API_KEY, apiKey);

		ResponseEntity responseEntity = null;
		if (isSucess) {
			responseEntity = new ResponseEntity(expResult, HttpStatus.OK);
		} else {
			responseEntity = new ResponseEntity(new IllegalArgumentException(expResult), HttpStatus.OK);
		}

		Mockito.when(restTemplate.getForEntity(requestUrl, Weather.class)).thenReturn(responseEntity);
		Weather resWeather = openWeatherReader.getCurrentWeather(location);

		assertNotNull(resWeather);
		if (isSucess) {
			assertEquals(resWeather.getMessage(), expResult);
		}else {
			assertEquals(resWeather.getMessage(), WeatherCheckConstants.LOC_ERROR_MESSAGE+location);
		
		}
	}

	@DataProvider(name = "locationProvider")
	public Iterator<Object[]> locationProvider() {
		List<Object[]> locations = new ArrayList<Object[]>();
		Object[] location1 = new Object[3];
		location1[0] = "";
		location1[1] = false;
		location1[2] = "Invalid location";
		locations.add(location1);

//		Object[] location2 = new Object[3];
//		location2[0] = "London";
//		location2[1] = true;
//		location2[2] = "Temp 2.0";
//		locations.add(location2);

//		Object[] location3 = new Object[3];
//		location3[0] = null;
//		location3[1] = false;
//		location3[2] = "Invalid location";
//		locations.add(location3);

		Object[] location4 = new Object[3];
		location4[0] = "Invalid";
		location4[1] = false;
		location4[2] = "Invalid location";
		locations.add(location4);

		return locations.iterator();
	}

}
