package com.performio.assigment.weathercheck.controller;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ValueNode;
import com.performio.assigment.weathercheck.AbstractWeatherCheckTest;
import com.performio.assigment.weathercheck.common.WeatherCheckConstants;

public class HealthCheckControllerTest extends AbstractWeatherCheckTest {

	@Autowired
	private WebApplicationContext webApplicationContext;

	protected MockMvc mvc;

	@BeforeClass
	protected void setUp() throws IllegalAccessException {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

	}

	@Test
	public void getHealthMetrixTest() throws Exception {
		String uri = WeatherCheckConstants.HEALTH_CHECK_API_PATH;
		MockHttpServletRequestBuilder req = MockMvcRequestBuilders.get(uri);
		MockHttpServletResponse response = mvc.perform(req.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn()
				.getResponse();
		assertEquals(response.getStatus(), HttpStatus.OK.value());
		ObjectMapper objectMapper = new ObjectMapper();
		JsonParser jsonParser = objectMapper.createParser(response.getContentAsString());
		TreeNode tree = jsonParser.readValueAsTree();
		ValueNode status = (ValueNode) tree.get("status");
		assertEquals("UP", status.asText());
	}
}
