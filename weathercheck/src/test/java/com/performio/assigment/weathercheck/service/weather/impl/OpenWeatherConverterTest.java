package com.performio.assigment.weathercheck.service.weather.impl;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.performio.assigment.weathercheck.model.Weather;

public class OpenWeatherConverterTest {

	@Test(dataProvider = "OpenWeatherConverterDataProvider")
	public void convertTest(double temp, String description) {
		OpenWeatherResponse openWeatherResponse = new OpenWeatherResponse();
		OpenMain main = new OpenMain();
		main.setTemp(temp);
		openWeatherResponse.setMain(main);
		OpenWeather[] openWeather = new OpenWeather[]{new OpenWeather()};
		openWeather[0].setDescription(description);
		openWeatherResponse.setWeather(openWeather);
		OpenWeatherConverter openWeatherConverter = new OpenWeatherConverter();
		Weather weatherRes = openWeatherConverter.convert(openWeatherResponse);
		assertEquals(openWeatherResponse.getMain().getTemp(), weatherRes.getTemp());
		assertEquals(openWeatherResponse.getWeather()[0].getDescription(), weatherRes.getDescription());
	}

	@DataProvider(name = "OpenWeatherConverterDataProvider")
	private ListIterator<Object[]> openWeatherConverterDataProvider() {
		List<Object[]> dataSetArray = new ArrayList<Object[]>();

		
		Object[] dataSet2 = new Object[2];
		dataSet2[0] = 0;
		dataSet2[1] = "";
		dataSetArray.add(dataSet2);

		
//		Object[] dataSet4 = new Object[2];
//		dataSet4[0] = null;
//		dataSet4[1] = null;
//		dataSetArray.add(dataSet4);
		
		Object[] dataSet5 = new Object[2];
		dataSet5[0] = 32;
		dataSet5[1] = null;
		dataSetArray.add(dataSet5);

		
//		Object[] dataSet6 = new Object[2];
//		dataSet6[0] = null;
//		dataSet6[1] = "description";
//		dataSetArray.add(dataSet6);
		
		Object[] dataSet7 = new Object[2];
		dataSet7[0] = 32;
		dataSet7[1] = "description";
		dataSetArray.add(dataSet7);
		
		return dataSetArray.listIterator();
	}
}
