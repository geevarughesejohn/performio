package com.performio.assigment.weathercheck.controller;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.performio.assigment.weathercheck.AbstractWeatherCheckTest;
import com.performio.assigment.weathercheck.WeathercheckApplication;
import com.performio.assigment.weathercheck.common.WeatherCheckConstants;
import com.performio.assigment.weathercheck.model.Weather;
import com.performio.assigment.weathercheck.service.weather.WeatherChecker;
import com.performio.assigment.weathercheck.service.weather.impl.OpenWeatherReader;

@SpringBootTest(classes = WeathercheckApplication.class)
@TestPropertySource(locations = "classpath:application-test.properties")
public class WeatherCheckControllerTest extends AbstractWeatherCheckTest {
	// TODO testcase in progress.. to be completed

	private static final int TEMP = 32;
	private static final String INVALID_LOCATION = "Invalid location";
	private static final String VALIDLOACATION = "validLoacation";
	private static final String TEMP_DESC = "Normal temp";

	@Autowired
	private WebApplicationContext webApplicationContext;

	protected MockMvc mvc;

	@Value("${apiKey}")
	private String apiKey;

	@BeforeClass
	protected void setUp() throws IllegalAccessException {
		OpenWeatherReader openWeatherReader = new OpenWeatherReader() {
			public com.performio.assigment.weathercheck.model.Weather getCurrentWeather(String location) {
				Weather weather = new Weather();
				if (VALIDLOACATION.equals(location)) {
					weather.setDescription(TEMP_DESC);
					weather.setTemp(TEMP);
					weather.setSuccess(true);
				} else {
					weather.setMessage(INVALID_LOCATION);
					weather.setSuccess(false);
				}
				return weather;
			};
		};
		FieldUtils.writeField(openWeatherReader, "apiKey", apiKey, true);
		WeatherChecker watherChecker = (WeatherChecker) webApplicationContext.getBean("weatherChecker");

		FieldUtils.writeField(watherChecker, "weatherReader", openWeatherReader, true);
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test(dataProvider = "locationProvider")
	public void weatherTest(String location, String expResult) throws Exception {

		String uri = WeatherCheckConstants.WEATHER_CHECK_API_PATH;
		MockHttpServletRequestBuilder req = MockMvcRequestBuilders.get(uri);
		req.param("location", location);
		MockHttpServletResponse response = mvc.perform(req.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn()
				.getResponse();
		ObjectMapper objectMapper = new ObjectMapper();
		JsonParser jsonParser = objectMapper.createParser(response.getContentAsString());
		Weather weather = jsonParser.readValueAs(Weather.class);
		assertEquals(weather.getMessage(), expResult);
		if (INVALID_LOCATION.equals(expResult)) {
			assertFalse(weather.isSuccess());
			assertEquals(weather.getMessage(), expResult);
//			assertEquals(weather.getDescription(), TEMP_DESC);
		} else {
			assertTrue(weather.isSuccess());
			assertEquals(weather.getTemp(), TEMP);
		}

	}

	@DataProvider(name = "locationProvider")
	public Iterator<Object[]> locationProvider() {
		List<Object[]> locations = new ArrayList<Object[]>();
		Object[] location1 = new Object[2];
		location1[0] = "";
		location1[1] = INVALID_LOCATION;
		locations.add(location1);

//		Object[] location2 = new Object[2];
//		location2[0] = VALIDLOACATION;
//		location2[1] = VALIDLOACATION;
//		locations.add(location2);

		Object[] location3 = new Object[2];
		location3[0] = null;
		location3[1] = INVALID_LOCATION;
		locations.add(location3);

		Object[] location4 = new Object[2];
		location4[0] = INVALID_LOCATION;
		location4[1] = INVALID_LOCATION;
		locations.add(location4);

		return locations.iterator();
	}
}
