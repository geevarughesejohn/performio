package com.performio.assigment.weathercheck.service.weather.impl;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.performio.assigment.weathercheck.common.WeatherCheckConstants;
import com.performio.assigment.weathercheck.model.Weather;
import com.performio.assigment.weathercheck.service.weather.WeatherConverter;
import com.performio.assigment.weathercheck.service.weather.WeatherReader;

/**
 * This class is an implementation of WeatherReader. Its reading weather
 * information form OpenWeather service.
 * <p/>
 * <ul>
 * For accessing OpenWeather service, should have
 * <li>OpenWeather service api key and that key should be set as
 * 'weathercheck.openWeather.apiKey' property.
 * <li>'weathercheck.openWeather.baseURL' - OpenWeather base url
 * </ul>
 * 
 * @author geevarughesejohn
 * @since 13 Nov 2021
 *
 */
@Component(value = "OpenWeatherReader")
public class OpenWeatherReader implements WeatherReader {

	private static Logger logger = LoggerFactory.getLogger(OpenWeatherReader.class);

	/**
	 * openWeather service base url
	 */
	@Value("${weathercheck.openWeather.baseURL}")
	private String baseURI;

	/**
	 * openWeather service api key
	 */
	@Value("${weathercheck.openWeather.apiKey}")
	private String apiKey;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private WeatherConverter<OpenWeatherResponse> openWeatherConverter;

	private URI uri;

	@PostConstruct
	public void init() throws Exception {
		this.uri = new URI(baseURI);
	}

	/**
	 * This methods reads the weather information from external open weather api and
	 * converting to weather format.
	 */
	public Weather getCurrentWeather(String location) {

		Map<String, String> params = new HashMap<String, String>();
		params.put(WeatherCheckConstants.CITY_NAME, location);
		params.put(WeatherCheckConstants.API_KEY, apiKey);
		OpenWeatherResponse openWeather = restTemplate.getForObject(getCurrentWeatherURI(), OpenWeatherResponse.class,
				params);
		if (openWeather == null) {
			return getErrorWeatherInfo(location);

		}
		return openWeatherConverter.convert(openWeather);
	}

	private Weather getErrorWeatherInfo(String location) {
		Weather weather = new Weather();
		weather.setMessage(WeatherCheckConstants.LOC_ERROR_MESSAGE + location);
		weather.setSuccess(false);
		return weather;

	}

	/**
	 * @return uri = base uri+ service path
	 */
	public String getCurrentWeatherURI() {
		return this.uri + WeatherCheckConstants.OPEN_WEATHER_API_PATH;

	}

}
