package com.performio.assigment.weathercheck;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * Spring boot application starter class
 * 
 * @author geevarughesejohn
 * @since 13 Nov 2021
 *
 */
@SpringBootApplication
@EnableSwagger2
public class WeathercheckApplication {

	private static Logger logger = LoggerFactory.getLogger(WeathercheckApplication.class);

	/**
	 * common rest client to access external rest apis.
	 * 
	 * @return RestTemplate
	 */
	@Bean
	public RestTemplate getRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		logger.info("RestTemplate bean created...");
		return restTemplate;

	}

	public static void main(String[] args) {
		SpringApplication.run(WeathercheckApplication.class, args);
		logger.info("Weathercheck Application started...");
	}

}