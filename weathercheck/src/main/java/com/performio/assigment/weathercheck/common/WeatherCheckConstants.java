package com.performio.assigment.weathercheck.common;

/**
 * 
 * WeatherCheck Constants class
 * 
 * @author geevarughesejohn
 * @since 13 Nov 2021
 *
 */
public final class WeatherCheckConstants {

	private WeatherCheckConstants() {
	};

	public static final String WEATHER_CHECK_API_PATH = "/weather";
	public static final String HEALTH_CHECK_API_PATH = "/healthcheck";
	public static final String ACTUATOR_HEALTH_CHECK_API_PATH = "/actuator/health";

	public static final String CITY_NAME = "city_name";
	public static final String API_KEY = "API_key";
	public static final String OPEN_WEATHER_API_PATH = "/data/2.5/weather?q={" + CITY_NAME + "}&appid={" + API_KEY
			+ "}";
	public static final String TXN_ID_KEY = "txn.id";
	
	public static final String LOC_ERROR_MESSAGE = "Weather information not found for location ";

}
