package com.performio.assigment.weathercheck.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.HealthComponent;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.performio.assigment.weathercheck.common.WeatherCheckConstants;

/**
 * 
 * HealthCheckController class
 * 
 * @author geevarughesejohn
 * @since 13 Nov 2021
 *
 */
@RestController
@RequestMapping(WeatherCheckConstants.HEALTH_CHECK_API_PATH)
public class HealthCheckController {

	private static Logger logger = LoggerFactory.getLogger(WeatherCheckController.class);

	@Autowired
	private HealthEndpoint healthEndpoint;

	/**
	 * @return
	 */
	@GetMapping
	public HealthComponent getHealthMetrix() {
		logger.debug("Request recived for health check");
		HealthComponent healthComponents = healthEndpoint.health();
		return healthComponents;
	}

}
