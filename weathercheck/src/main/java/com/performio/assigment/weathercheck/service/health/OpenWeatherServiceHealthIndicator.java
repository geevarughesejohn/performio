package com.performio.assigment.weathercheck.service.health;

import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class OpenWeatherServiceHealthIndicator implements HealthIndicator {

	private static Logger logger = LoggerFactory.getLogger(OpenWeatherServiceHealthIndicator.class);
	/**
	 * openWeather service base url
	 */
	@Value("${weathercheck.openWeather.baseURL}")
	private String baseURI;

	public Health health() {
		try (Socket socket = new Socket(new java.net.URL(baseURI).getHost(), 80)) {
		} catch (Exception e) {
			logger.warn("Failed to connect to: {}", baseURI);
			return Health.down().withDetail("error", e.getMessage()).build();
		}
		return Health.up().build();
	}

}