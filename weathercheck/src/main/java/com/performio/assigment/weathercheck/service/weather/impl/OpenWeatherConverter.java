package com.performio.assigment.weathercheck.service.weather.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.performio.assigment.weathercheck.model.Weather;
import com.performio.assigment.weathercheck.service.weather.WeatherConverter;

/**
 * Implementation of WeatherConverter for @OpenWeatherResponse
 * 
 * 
 * @author geevarughesejohn
 * @since 13 Nov 2021
 *
 */
@Component
public class OpenWeatherConverter implements WeatherConverter<OpenWeatherResponse> {

	private static Logger logger = LoggerFactory.getLogger(OpenWeatherConverter.class);

	/**
	 * convert OpenWeatherResponse to Weather if any exception, return Weather with
	 * isSuccess = false and error message
	 */
	public Weather convert(OpenWeatherResponse openWeatherResponse) {
		Weather weather = new Weather();
		try {
			weather.setMessage(null);
			weather.setTemp(openWeatherResponse.getMain().getTemp());
			if (openWeatherResponse.getWeather().length > 0) {
				weather.setDescription(openWeatherResponse.getWeather()[0].getDescription());
			}
			weather.setSuccess(true);
			logger.debug("openWeatherResponse converted to Weather");
		} catch (Exception e) {
			weather.setMessage(e.getMessage());
			weather.setSuccess(false);
			logger.debug("failed to converte openWeatherResponse to Weather");
		}
		logger.debug("OpenWeatherResponse : " + openWeatherResponse + "\n Weather: " + weather);

		return weather;
	}

}
