package com.performio.assigment.weathercheck.service.weather.impl;

/**
 * Weather service response class. Pojo class to capture Open weather api
 * response
 * 
 * @author geevarughesejohn
 * @since 13 Nov 2021
 *
 */
public class OpenWeather {

	private String description;

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}