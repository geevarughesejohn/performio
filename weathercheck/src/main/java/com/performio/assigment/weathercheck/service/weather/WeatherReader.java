package com.performio.assigment.weathercheck.service.weather;

import com.performio.assigment.weathercheck.model.Weather;

/**
 * Interface to define weather readers
 * 
 * @author geevarughesejohn
 * @since 13 Nov 2021
 *
 */
public interface WeatherReader {

	/**
	 * read and return weather information
	 * 
	 * @param location
	 * @return Weather for valid location. if location is invalid, return Weather
	 *         with error message
	 */
	Weather getCurrentWeather(String location);

}
