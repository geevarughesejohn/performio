package com.performio.assigment.weathercheck.service.health;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.CompositeHealthContributor;
import org.springframework.boot.actuate.health.HealthContributor;
import org.springframework.boot.actuate.health.NamedContributor;
import org.springframework.stereotype.Component;

@Component("WeatherAPI")
public class WeatherAPIHealthContributor implements CompositeHealthContributor {

	private Map<String, HealthContributor> contributors = new LinkedHashMap<>();

	@Autowired
	public WeatherAPIHealthContributor(OpenWeatherServiceHealthIndicator openWeatherServiceHealthIndicator) {

		contributors.put("openWeatherService", openWeatherServiceHealthIndicator);

	}

	/**
	 * return list of health contributors
	 */
	@Override
	public Iterator<NamedContributor<HealthContributor>> iterator() {
		return contributors.entrySet().stream().map((entry) -> NamedContributor.of(entry.getKey(), entry.getValue()))
				.iterator();
	}

	@Override
	public HealthContributor getContributor(String name) {
		return contributors.get(name);
	}

}