package com.performio.assigment.weathercheck.service.weather.impl;

/**
 * OpenWeather Response pojo class
 * 
 * @author geevarughesejohn
 * @since 13 Nov 2021
 *
 */
public class OpenWeatherResponse {

	private OpenWeather[] weather;
	
	private OpenMain main;

	public OpenWeather[] getWeather() {
		return weather;
	}

	public void setWeather(OpenWeather[] weather) {
		this.weather = weather;
	}

	public OpenMain getMain() {
		return main;
	}

	public void setMain(OpenMain main) {
		this.main = main;
	}



	
}