package com.performio.assigment.weathercheck.service.weather.impl;

/**
 * OpenMain class -Pojo class to capture Open weather api response
 * 
 * @author geevarughesejohn
 * @since 13 Nov 2021
 *
 */
public class OpenMain {
	private double temp;

	public double getTemp() {
		return temp;
	}

	public void setTemp(double temp) {
		this.temp = temp;
	}
}