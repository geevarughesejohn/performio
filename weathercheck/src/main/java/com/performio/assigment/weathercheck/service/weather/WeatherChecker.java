package com.performio.assigment.weathercheck.service.weather;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.common.base.Strings;
import com.performio.assigment.weathercheck.model.Weather;

/**
 * WeatherChecker service. This is responsible service class to read weather
 * information from weatherReader.
 * 
 * @author geevarughesejohn
 * @since 13 Nov 2021
 *
 */
@Service
public class WeatherChecker {

	private static Logger logger = LoggerFactory.getLogger(WeatherChecker.class);

	@Autowired
	@Qualifier("OpenWeatherReader")
	private WeatherReader weatherReader;

	/**
	 * Getting the Weather information of the given location
	 * 
	 * if location is invalid, return Weather with message
	 * 
	 * @param location
	 * @return
	 */
	public Weather getCurrentWeather(String location) {
		Weather weather;
		if (!isValidLocation(location)) {
			logger.debug("Reading CurrentWeather.. But location is invalid:" + location);
			weather = new Weather();
			weather.setSuccess(false);
			weather.setMessage("Invalid location");
			return weather;
		} else {
			logger.debug("Reading CurrentWeather for location:" + location);
			weather = weatherReader.getCurrentWeather(location);
		}
		return weather;

	}

	private boolean isValidLocation(String location) {
		return !Strings.isNullOrEmpty(location);
	}

}
