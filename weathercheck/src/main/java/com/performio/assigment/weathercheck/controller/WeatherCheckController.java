package com.performio.assigment.weathercheck.controller;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.performio.assigment.weathercheck.common.WeatherCheckConstants;
import com.performio.assigment.weathercheck.model.Weather;
import com.performio.assigment.weathercheck.service.weather.WeatherChecker;

/**
 * 
 * WeatherCheckController class
 * 
 * @author geevarughesejohn
 * @since 13 Nov 2021
 *
 */
@RestController
@RequestMapping(WeatherCheckConstants.WEATHER_CHECK_API_PATH)
public class WeatherCheckController {

	private static Logger logger = LoggerFactory.getLogger(WeatherCheckController.class);

	@Autowired
	private WeatherChecker weatherChecker;

	/**
	 * GET method.
	 * 
	 * @param location - location for weather check
	 * @return - current weather of given location.
	 */
	@GetMapping
	public Weather weather(String location) {
		MDC.put(WeatherCheckConstants.TXN_ID_KEY, UUID.randomUUID().toString());
		logger.debug("Request recived to check the weather of location: " + location);
		Weather weather = weatherChecker.getCurrentWeather(location);
		logger.debug("Response for the request" + weather);
		MDC.clear();
		return weather;
	}

}
