package com.performio.assigment.weathercheck.service.weather;

import com.performio.assigment.weathercheck.model.Weather;

/**
 * Interface to convert external weather information to @Weather
 * 
 * @author geevarughesejohn
 * @since 13 Nov 2021
 *
 */
public interface WeatherConverter<T> {

	/**
	 * convert external weather information to Weather object
	 * 
	 * @param weatherInfo
	 * @return Weather
	 */
	Weather convert(T weatherInfo);

}
